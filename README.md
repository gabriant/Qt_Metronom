# Projekt Metronom

Jednoduchý metronom se základními zvuky, doplněný o zvuky bicích, se kterými je možné vytvářet různé doprovody kombinacemi nastavení metronomu.

## Funkce

- **Základní metronom**: Nastavení tempa (BPM) s různými zvuky metronomu pro udržování rytmu.
- **Bicí doprovody**: Možnost přehrávání různých bicí doprovodů podle zvolených funkcí.
- **Tlačítko Tap**: Umožňuje uživatelům nastavit BPM podle intervalu mezi kliknutími.

## Stěžejní knihovny

- **QSoundEffect**: Použitá pro přehrávání zvuků. (https://doc.qt.io/qt-6/qsoundeffect.html)
- **QTimer**: Časovač, který se používá jak pro intervaly mezi dobami, tak i pro uživatelské rozhraní a efekty tlačítek.
- Další knihovny: Pro implementaci uživatelského rozhraní a dalších funkcí metronomu.

## Video ukázka
https://youtu.be/eEJIem602gE?si=z7zEnXIHY2AXz0bV
