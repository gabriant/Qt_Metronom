#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>
#include <vector>
#include <chrono>
#include <QIcon>
#include <QSoundEffect>
#include <QPalette>

//int for note type (shufflee, triola ....)
int MainWindow::TypNoty = 1;

//bool for number of notes 8
bool MainWindow::lastsnare = true;

//vector with name of sounds (also for paths)
std::vector<QString> sounds = {"Traditional", "Digital", "Woodblock", "Drums", "Voices"};



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::demo_Metronome)
    , timer(new QTimer(this))
{
    ui->setupUi(this);

    //Play button and function changeplaybuttonText connection
    connect(ui->playbutton, SIGNAL(clicked()), this, SLOT(changeplaybuttonText()));

    //Icons setup
    ui->ctvrtina->setIcon(QIcon(":/recources/Images/ctvrtova.png"));
    ui->ctvrtina->setIconSize(QSize(64, 64));
    ui->osmina->setIcon(QIcon(":/recources/Images/osmina.png"));
    ui->osmina->setIconSize(QSize(64, 64));
    ui->triola->setIcon(QIcon(":/recources/Images/triola.png"));
    ui->triola->setIconSize(QSize(64, 64));
    ui->shuffle->setIcon(QIcon(":/recources/Images/shuffle.png"));
    ui->shuffle->setIconSize(QSize(64, 64));
    QString NotestyleSheet = QString("border: 3px solid black; padding: 3px;");
    ui->ctvrtina->setStyleSheet("border: 2px solid gray; padding: 3px;");
    ui->osmina->setStyleSheet(NotestyleSheet);
    ui->triola->setStyleSheet(NotestyleSheet);
    ui->shuffle->setStyleSheet(NotestyleSheet);







}

MainWindow::~MainWindow()
{
    delete ui;
}


//Change text of button and play metronom
void MainWindow::changeplaybuttonText()
{
    QPushButton* button = ui->playbutton;
    if (button->text() == "PLAY") {
        button->setText("STOP");
        playMetronome();
    } else {
        button->setText("PLAY");
        timer->stop();
        ui->doba->setText("");
    }
}


//Play metronom -> timer with bpm and call right function
void MainWindow::playMetronome()
{
    disconnect(timer, &QTimer::timeout, nullptr, nullptr);
    static int i = 0;
    i = 0;

    double bpm = static_cast<double>(ui->bpmchange->value());
    int interval = static_cast<int>(60000.0 / bpm);

    connect(timer, &QTimer::timeout, this, [this, interval]()
            {
                int pocetDob = ui->pocetDobSpin->value();
                i = (i + 1) % pocetDob;
                i = (i == 0) ? pocetDob : i;

                //Change color of main sqaure
                colorsChange(i, interval);

                // Play sound based on type of sound
                if(ui->Sound_Spin->value() <= 3)
                {
                    classicmetronome(i, interval);
                }
                if(ui->Sound_Spin->value() == 5)
                {
                    voicemetronome(i, interval);
                }
                if(ui->Sound_Spin->value() == 4)
                {
                    classicmetronome(i, interval);
                    drums(i);
                }

            });
    timer->start(interval);
}


//function for changing color of main square
void MainWindow::colorsChange(int dobaN, int interval)
{
    // Set color based on dobaN
    QString barva = (dobaN == 1) ? "rgb(255, 245, 73)" : "rgb(28, 177, 219)";
    QString styleSheet = QString("border: 6px solid %1; color: white; text-align: center; font-size: 60px; font-weight: bold; padding: 0px 0px 0px 58px; background-color: black;").arg(barva);

    if (dobaN > 9)
    {
        styleSheet = QString("border: 6px solid %1; color: white; text-align: center; font-size: 60px; font-weight: bold; padding: 0px 0px 0px 40px; background-color: black;").arg(barva);
    }

    ui->doba->setStyleSheet(styleSheet);
    ui->doba->setText(QString::number(dobaN));

    //Change color back in half time (flicker effect)
    QTimer::singleShot(interval / 2, [this, dobaN]()
    {
        QString grayStyleSheet = QString("border: 6px solid gray; color: white; text-align: center; font-size: 60px; font-weight: bold; padding: 0px 0px 0px 58px; background-color: black;");
        if (dobaN > 9)
        {
            grayStyleSheet = QString("border: 6px solid gray; color: white; text-align: center; font-size: 60px; font-weight: bold; padding: 0px 0px 0px 40px; background-color: black;");
        }
        ui->doba->setStyleSheet(grayStyleSheet);
    });



}


//Play classic metronome only for traditional, digital, woodblock and for hihat
void MainWindow::classicmetronome(int dobaN, int interval)
{
    int sound = ui->Sound_Spin->value();
    auto playSound = [this, dobaN, sound](bool whichsound)
    {
        QString oneSound = ((dobaN == 1 && whichsound) || sound == 1 || sound == 4)? "/first.wav" : "/second.wav";
        QString soundpath = QString(":/recources/metronome/") + sounds[sound - 1] + oneSound;
        QSoundEffect *soundEffect = new QSoundEffect(this);
        soundEffect->setSource(QUrl::fromLocalFile((soundpath)));
        soundEffect->setVolume(ui->volume->value()/100.0);
        soundEffect->play();
        connect(soundEffect, &QSoundEffect::playingChanged, soundEffect, &QSoundEffect::deleteLater);
    };

    //play fisrt
      playSound(true);

    //play other depending on type of note
    switch (TypNoty) {
    case 2:
        QTimer::singleShot(interval / 2, this, [=]() { playSound(false); });
        break;
    case 3:
        QTimer::singleShot(interval / 3, this, [=]() {
            playSound(false);
            QTimer::singleShot(interval / 3, this, [=]() { playSound(false); });
        });
        break;
    case 4:
        QTimer::singleShot(interval / 3, this, [=]() {
            if (sound == 4)
            {
                //when drums -> play ghostnote
                QSoundEffect *soundEffect = new QSoundEffect(this);
                soundEffect->setSource(QUrl::fromLocalFile((":/recources/metronome/Drums/ghostnote.wav")));
                soundEffect->setVolume(ui->volume->value()/100.0);
                soundEffect->play();
                connect(soundEffect, &QSoundEffect::playingChanged, soundEffect, &QSoundEffect::deleteLater);
            }

            QTimer::singleShot(interval / 3, this, [=]()
            {
                playSound(false);
                //add kick when is shuffle and drums sound
                if(dobaN % 4 == 0 && sound == 4)
                {
                    QSoundEffect *soundEffect = new QSoundEffect(this);
                    soundEffect->setSource(QUrl::fromLocalFile((":/recources/metronome/Drums/kick.wav")));
                    soundEffect->setVolume(ui->volume->value()/100.0);
                    soundEffect->play();
                    connect(soundEffect, &QSoundEffect::playingChanged, soundEffect, &QSoundEffect::deleteLater);
                }
            });
        });
        break;
    default:
        break;
    }
}



//voice metronome
void MainWindow::voicemetronome(int dobaN, int interval)
{
    int sound = ui->Sound_Spin->value();
    auto playSound = [this, dobaN, sound]()
    {
        QString soundpath = QString(":/recources/metronome/Woodblock/second.wav");
        QSoundEffect *soundEffect = new QSoundEffect(this);
        soundEffect->setSource(QUrl::fromLocalFile(soundpath));
        soundEffect->setVolume(ui->volume->value()/200.0);
        soundEffect->play();
        connect(soundEffect, &QSoundEffect::playingChanged, soundEffect, &QSoundEffect::deleteLater);
    };


    //play voice with woodblock
    QString soundpath = QString(":/recources/metronome/Voices/" + QString::number(dobaN) + ".wav");
    QSoundEffect *soundEffect = new QSoundEffect(this);
    soundEffect->setSource(QUrl::fromLocalFile(soundpath));
    soundEffect->setVolume(ui->volume->value()/100.0);
    soundEffect->play();
    connect(soundEffect, &QSoundEffect::playingChanged, soundEffect, &QSoundEffect::deleteLater);
    playSound();


    //play only woodblock depending on type of note
    switch (TypNoty) {
    case 2:
        QTimer::singleShot(interval / 2, this, [=]() { playSound(); });
        break;
    case 3:
        QTimer::singleShot(interval / 3, this, [=]() {
            playSound();
            QTimer::singleShot(interval / 3, this, [=]() { playSound(); });
        });
        break;
    case 4:
        QTimer::singleShot(interval / 3, this, [=]() {
            QTimer::singleShot(interval / 3, this, [=]() { playSound(); });
        });
        break;
    default:
        break;
    }
}


//drums
void MainWindow::drums(int dobaN)
{
    auto playSound = [this](bool kick)
    {
        QString soundpath = QString(":/recources/metronome/Drums/" + QString(kick ? "kick" : "snare") + ".wav");
        QSoundEffect *soundEffect = new QSoundEffect(this);
        soundEffect->setSource(QUrl::fromLocalFile(soundpath));
        soundEffect->setVolume(ui->volume->value()/100.0);
        soundEffect->play();
        connect(soundEffect, &QSoundEffect::playingChanged, soundEffect, &QSoundEffect::deleteLater);
    };

    //play kick/snare depending on set number of notes
    switch (ui->pocetNoteSpin->value()) {
    case 2:
        if ((dobaN + 1) % 2 == 0)
        {
            playSound(true);
        }
        if ((dobaN) % 2 == 0)
        {
            playSound(false);
        }
        break;
    case 4:
        if ((dobaN - 1) % 4 == 0)
        {
            playSound(true);
        }
        if ((dobaN - 3) % 4 == 0)
        {
            playSound(false);
        }
        break;
    case 8:
        if (dobaN==1)
        {
            playSound(lastsnare);
            lastsnare = !lastsnare;
        }

        break;
    default:
        break;
    }
}



//functin for setting BPM depending on clicking on tap button
void MainWindow::on_TAP_clicked()
{

    QString styleSheet1 = QString("margin: 10px 30px; background-color: black; border: 2px solid black; color: white; text-align: center; text-decoration: none; font-color: white; font-size: 20px; font-weight: bold;");
    ui->TAP->setStyleSheet(styleSheet1);
    static int clicked = 0;
    static qint64 total_time = 0;
    static qint64 last_time = 0;
    QString styleSheet = QString("border: 1px solid black; font-weight: bold; font-size: 15px; padding: 0 0 0 25px;");
    ui->BPM_TEXT->setStyleSheet(styleSheet);
    QTimer::singleShot(100, [this]()
    {
        QString styleSheet1 = QString("margin: 10px 30px; background-color: black; border: 2px solid gray; color: white; text-align: center; text-decoration: none; font-color: white; font-size: 20px; font-weight: bold;");
        ui->TAP->setStyleSheet(styleSheet1);
    });



    qint64 time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    if (clicked == 0 || (time - last_time) >= 3000 )
    {
        clicked = 0;
        last_time = 0;
        total_time = 0;
        ui->BPM_TEXT->clear();
        ui->BPM_TEXT->setText("TAP!");
    }

    if (clicked == 0)
    {
        last_time = time;
    }
    else
    {
        total_time += (time - last_time);
        last_time = time;
    }

    clicked++;

    if (clicked == 4)
    {
        qint64 average_time = total_time / 3;
        double bpm = 60000.0 / average_time;
        ui->bpmchange->setValue(static_cast<int>(bpm));
        clicked = 0;
        total_time = 0;
        last_time = 0;
    }
}



//change bpm on bpmchange value changed
void MainWindow::on_bpmchange_valueChanged(int arg1)
{
    QString styleSheet = QString("border: 1px solid black; font-weight: bold; font-size: 15px; padding: 0 0 0 7px;");
    ui->BPM_TEXT->setStyleSheet(styleSheet);
    ui->BPM_TEXT->clear();
    ui->BPM_TEXT->setText(QString::number(arg1) + " BPM");
    int interval = static_cast<int>(60000.0 / arg1);
    timer->stop();
    timer->setInterval(interval);

    if (ui->playbutton->text() == "STOP")
    {

       playMetronome();
    }

}


//add bpm to bpm square
void MainWindow::on_BPM_TEXT_textChanged()
{
    if (ui->BPM_TEXT->toPlainText() == "TAP!")
    {
        QTimer::singleShot(15000, [this]()
        {
            if (ui->BPM_TEXT->toPlainText() == "TAP!")
            {
                QString styleSheet = QString("border: 1px solid black; font-weight: bold; font-size: 15px; padding: 0 0 0 7px;");
                ui->BPM_TEXT->setStyleSheet(styleSheet);
                ui->BPM_TEXT->clear();
                int bpm = ui->bpmchange->value();
                ui->BPM_TEXT->setText(QString::number(bpm) + " BPM");
            }
        });
    }
}




//change of number of times
void MainWindow::on_pocetDobSpin_valueChanged(int arg1)
{
    QString styleSheet = QString("border: 1px solid black; font-weight: bold; font-size: 15px; padding: 0 0 0 23px");
    ui->TS_TEXT->setStyleSheet(styleSheet);
    ui->TS_TEXT->setText(QString::number(arg1) + " / " + QString::number(ui->pocetNoteSpin->value()));
}


//change of number of notes
void MainWindow::on_pocetNoteSpin_valueChanged(int arg1)
{
    int value = arg1;
    if (value != 2 && value != 4 && value != 8) {
        if (value < 4) {
            ui->pocetNoteSpin->setValue(2);
        } else if (value < 8) {
            ui->pocetNoteSpin->setValue(4);
        } else {
            ui->pocetNoteSpin->setValue(8);
        }
    }

    QString styleSheet = QString("border: 1px solid black; font-weight: bold; font-size: 15px; padding: 0 0 0 23px");
    ui->TS_TEXT->setStyleSheet(styleSheet);
    ui->TS_TEXT->setText(QString::number(ui->pocetDobSpin->value()) + " / " + QString::number(ui->pocetNoteSpin->value()));
}



//change type of note
void MainWindow::on_ctvrtina_clicked()
{
    QString NotestyleSheet = QString("border: 3px solid black; padding: 3px;");
    ui->ctvrtina->setStyleSheet("border: 2px solid gray; padding: 3px;");
    ui->osmina->setStyleSheet(NotestyleSheet);
    ui->triola->setStyleSheet(NotestyleSheet);
    ui->shuffle->setStyleSheet(NotestyleSheet);
    TypNoty = 1;
}

void MainWindow::on_osmina_clicked()
{
    QString NotestyleSheet = QString("border: 3px solid black; padding: 3px;");
    ui->ctvrtina->setStyleSheet(NotestyleSheet);
    ui->osmina->setStyleSheet("border: 2px solid gray; padding: 3px;");
    ui->triola->setStyleSheet(NotestyleSheet);
    ui->shuffle->setStyleSheet(NotestyleSheet);
    TypNoty = 2;
}

void MainWindow::on_triola_clicked()
{
    QString NotestyleSheet = QString("border: 3px solid black; padding: 3px;");
    ui->ctvrtina->setStyleSheet(NotestyleSheet);
    ui->osmina->setStyleSheet(NotestyleSheet);
    ui->triola->setStyleSheet("border: 2px solid gray; padding: 3px;");
    ui->shuffle->setStyleSheet(NotestyleSheet);
    TypNoty = 3;
}

void MainWindow::on_shuffle_clicked()
{
    QString NotestyleSheet = QString("border: 3px solid black; padding: 3px;");
    ui->ctvrtina->setStyleSheet(NotestyleSheet);
    ui->osmina->setStyleSheet(NotestyleSheet);
    ui->triola->setStyleSheet(NotestyleSheet);
    ui->shuffle->setStyleSheet("border: 2px solid gray; padding: 3px;");
    TypNoty = 4;
}




//change volume
void MainWindow::on_Sound_Spin_valueChanged(int arg1)
{
    ui->SOUND_TEXT->setText(sounds[arg1 - 1]);
}

