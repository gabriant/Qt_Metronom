#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QSoundEffect>

QT_BEGIN_NAMESPACE
namespace Ui {
class demo_Metronome;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //some static
    static int TypNoty;
    static bool lastsnare;





private slots:

    void changeplaybuttonText();



    void playMetronome();
    void colorsChange(int dobaN, int interval);
    void classicmetronome(int dobaN, int interval);
    void voicemetronome(int dobaN, int interval);
    void drums(int dobaN);

    //tap function
    void on_TAP_clicked();

    //some signals
    void on_bpmchange_valueChanged(int arg1);

    void on_BPM_TEXT_textChanged();

    void on_pocetDobSpin_valueChanged(int arg1);

    void on_pocetNoteSpin_valueChanged(int arg1);

    void on_ctvrtina_clicked();
    void on_osmina_clicked();
    void on_triola_clicked();
    void on_shuffle_clicked();

    void on_Sound_Spin_valueChanged(int arg1);

private:
    Ui::demo_Metronome *ui;

    //timer
    QTimer *timer;

protected:
};
#endif // MAINWINDOW_H
