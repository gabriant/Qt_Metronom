/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_demo_Metronome
{
public:
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *TAP;
    QPushButton *playbutton;
    QSpinBox *bpmchange;
    QTextEdit *Header;
    QTextEdit *doba;
    QTextEdit *BPM_TEXT;
    QTextEdit *TS_TEXT;
    QSpinBox *pocetDobSpin;
    QTextEdit *Beat;
    QTextEdit *Note;
    QSpinBox *pocetNoteSpin;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *ctvrtina;
    QPushButton *osmina;
    QPushButton *triola;
    QPushButton *shuffle;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpinBox *Sound_Spin;
    QTextEdit *SOUND_TEXT;
    QSlider *volume;

    void setupUi(QMainWindow *demo_Metronome)
    {
        if (demo_Metronome->objectName().isEmpty())
            demo_Metronome->setObjectName("demo_Metronome");
        demo_Metronome->resize(390, 570);
        demo_Metronome->setMinimumSize(QSize(390, 570));
        demo_Metronome->setMaximumSize(QSize(390, 570));
        demo_Metronome->setStyleSheet(QString::fromUtf8("background-color: rgb(33, 33, 33);"));
        centralwidget = new QWidget(demo_Metronome);
        centralwidget->setObjectName("centralwidget");
        centralwidget->setStyleSheet(QString::fromUtf8(""));
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName("horizontalLayoutWidget");
        horizontalLayoutWidget->setGeometry(QRect(10, 420, 371, 102));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(19);
        horizontalLayout->setObjectName("horizontalLayout");
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        TAP = new QPushButton(horizontalLayoutWidget);
        TAP->setObjectName("TAP");
        TAP->setMinimumSize(QSize(0, 70));
        TAP->setCursor(QCursor(Qt::PointingHandCursor));
        TAP->setStyleSheet(QString::fromUtf8("margin: 10px 30px;\n"
"background-color: black;\n"
"border: 2px solid gray;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"cursor: pointer;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;\n"
"\n"
""));

        horizontalLayout->addWidget(TAP);

        playbutton = new QPushButton(horizontalLayoutWidget);
        playbutton->setObjectName("playbutton");
        playbutton->setEnabled(true);
        playbutton->setMinimumSize(QSize(0, 100));
        playbutton->setCursor(QCursor(Qt::PointingHandCursor));
        playbutton->setStyleSheet(QString::fromUtf8("margin: 15px;\n"
"background-color: black;\n"
"border: 2px solid gray;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"cursor: pointer;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));

        horizontalLayout->addWidget(playbutton);

        bpmchange = new QSpinBox(horizontalLayoutWidget);
        bpmchange->setObjectName("bpmchange");
        bpmchange->setStyleSheet(QString::fromUtf8("background-color: black;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        bpmchange->setAlignment(Qt::AlignCenter);
        bpmchange->setMaximum(300);
        bpmchange->setStepType(QAbstractSpinBox::DefaultStepType);
        bpmchange->setValue(130);

        horizontalLayout->addWidget(bpmchange);

        Header = new QTextEdit(centralwidget);
        Header->setObjectName("Header");
        Header->setEnabled(false);
        Header->setGeometry(QRect(90, 10, 221, 51));
        Header->setStyleSheet(QString::fromUtf8("background-color: black;\n"
"border: 2px solid gray;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;"));
        doba = new QTextEdit(centralwidget);
        doba->setObjectName("doba");
        doba->setEnabled(false);
        doba->setGeometry(QRect(110, 80, 171, 111));
        doba->setAutoFillBackground(true);
        doba->setStyleSheet(QString::fromUtf8("text-align: center;\n"
"background-color: black;\n"
"border: 6px solid;\n"
"border-color: gray;\n"
"color: white;\n"
"\n"
"text-align: center; \n"
"text-decoration: none; \n"
"color: white;\n"
"font-size: 60px;\n"
"font-weight: bold;"));
        doba->setReadOnly(true);
        BPM_TEXT = new QTextEdit(centralwidget);
        BPM_TEXT->setObjectName("BPM_TEXT");
        BPM_TEXT->setEnabled(false);
        BPM_TEXT->setGeometry(QRect(290, 120, 91, 31));
        BPM_TEXT->setStyleSheet(QString::fromUtf8("border: 1px solid black; \n"
"font-weight: bold; \n"
"font-size: 12px; \n"
"padding: 0 0 0 7px;"));
        TS_TEXT = new QTextEdit(centralwidget);
        TS_TEXT->setObjectName("TS_TEXT");
        TS_TEXT->setEnabled(false);
        TS_TEXT->setGeometry(QRect(10, 120, 91, 31));
        TS_TEXT->setStyleSheet(QString::fromUtf8("border: 1px solid black; font-weight: bold; font-size: 15px; padding: 0 0 0 23px;"));
        pocetDobSpin = new QSpinBox(centralwidget);
        pocetDobSpin->setObjectName("pocetDobSpin");
        pocetDobSpin->setGeometry(QRect(100, 220, 91, 31));
        pocetDobSpin->setStyleSheet(QString::fromUtf8("background-color: black;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        pocetDobSpin->setAlignment(Qt::AlignCenter);
        pocetDobSpin->setMinimum(1);
        pocetDobSpin->setMaximum(16);
        pocetDobSpin->setValue(4);
        Beat = new QTextEdit(centralwidget);
        Beat->setObjectName("Beat");
        Beat->setEnabled(false);
        Beat->setGeometry(QRect(10, 220, 91, 31));
        Beat->setStyleSheet(QString::fromUtf8("background-color: transparent;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        Note = new QTextEdit(centralwidget);
        Note->setObjectName("Note");
        Note->setEnabled(false);
        Note->setGeometry(QRect(200, 220, 91, 31));
        Note->setStyleSheet(QString::fromUtf8("background-color: transparent;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        pocetNoteSpin = new QSpinBox(centralwidget);
        pocetNoteSpin->setObjectName("pocetNoteSpin");
        pocetNoteSpin->setGeometry(QRect(290, 220, 91, 31));
        pocetNoteSpin->setStyleSheet(QString::fromUtf8("background-color: black;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        pocetNoteSpin->setAlignment(Qt::AlignCenter);
        pocetNoteSpin->setReadOnly(false);
        pocetNoteSpin->setMinimum(2);
        pocetNoteSpin->setMaximum(8);
        pocetNoteSpin->setSingleStep(4);
        pocetNoteSpin->setStepType(QAbstractSpinBox::DefaultStepType);
        pocetNoteSpin->setValue(4);
        horizontalLayoutWidget_2 = new QWidget(centralwidget);
        horizontalLayoutWidget_2->setObjectName("horizontalLayoutWidget_2");
        horizontalLayoutWidget_2->setGeometry(QRect(10, 270, 371, 80));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        ctvrtina = new QPushButton(horizontalLayoutWidget_2);
        ctvrtina->setObjectName("ctvrtina");
        ctvrtina->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout_2->addWidget(ctvrtina);

        osmina = new QPushButton(horizontalLayoutWidget_2);
        osmina->setObjectName("osmina");
        osmina->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout_2->addWidget(osmina);

        triola = new QPushButton(horizontalLayoutWidget_2);
        triola->setObjectName("triola");
        triola->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout_2->addWidget(triola);

        shuffle = new QPushButton(horizontalLayoutWidget_2);
        shuffle->setObjectName("shuffle");
        shuffle->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout_2->addWidget(shuffle);

        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName("verticalLayoutWidget");
        verticalLayoutWidget->setGeometry(QRect(10, 350, 371, 71));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        Sound_Spin = new QSpinBox(verticalLayoutWidget);
        Sound_Spin->setObjectName("Sound_Spin");
        Sound_Spin->setMinimumSize(QSize(182, 0));
        Sound_Spin->setStyleSheet(QString::fromUtf8("background-color: black;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 20px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        Sound_Spin->setAlignment(Qt::AlignCenter);
        Sound_Spin->setMinimum(1);
        Sound_Spin->setMaximum(5);

        horizontalLayout_3->addWidget(Sound_Spin);

        SOUND_TEXT = new QTextEdit(verticalLayoutWidget);
        SOUND_TEXT->setObjectName("SOUND_TEXT");
        SOUND_TEXT->setEnabled(false);
        SOUND_TEXT->setMinimumSize(QSize(0, 20));
        SOUND_TEXT->setMaximumSize(QSize(16777215, 35));
        SOUND_TEXT->setLayoutDirection(Qt::LeftToRight);
        SOUND_TEXT->setAutoFillBackground(false);
        SOUND_TEXT->setStyleSheet(QString::fromUtf8("background-color: transparent;\n"
"color: white;\n"
"text-align: center; \n"
"text-decoration: none; \n"
"font-size: white;\n"
"font-size: 17px;\n"
"font-weight: bold;\n"
"cursor: pointer;"));
        SOUND_TEXT->setAutoFormatting(QTextEdit::AutoNone);
        SOUND_TEXT->setReadOnly(true);

        horizontalLayout_3->addWidget(SOUND_TEXT);


        verticalLayout->addLayout(horizontalLayout_3);

        volume = new QSlider(centralwidget);
        volume->setObjectName("volume");
        volume->setGeometry(QRect(20, 535, 351, 21));
        volume->setStyleSheet(QString::fromUtf8(""));
        volume->setMinimum(0);
        volume->setMaximum(100);
        volume->setValue(50);
        volume->setOrientation(Qt::Horizontal);
        demo_Metronome->setCentralWidget(centralwidget);

        retranslateUi(demo_Metronome);

        QMetaObject::connectSlotsByName(demo_Metronome);
    } // setupUi

    void retranslateUi(QMainWindow *demo_Metronome)
    {
        demo_Metronome->setWindowTitle(QCoreApplication::translate("demo_Metronome", "MainWindow", nullptr));
        TAP->setText(QCoreApplication::translate("demo_Metronome", "TAP", nullptr));
        playbutton->setText(QCoreApplication::translate("demo_Metronome", "PLAY", nullptr));
        Header->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:20pt; font-weight:700;\">METRONOME</span></p></body></html>", nullptr));
        doba->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:60px; font-weight:700; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", nullptr));
        BPM_TEXT->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:12px; font-weight:700; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">130 BPM</span></p></body></html>", nullptr));
        TS_TEXT->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:15px; font-weight:700; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">4 / 4</span></p></body></html>", nullptr));
        pocetDobSpin->setSuffix(QString());
        pocetDobSpin->setPrefix(QString());
        Beat->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:20px; font-weight:700; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">BEAT:</span></p></body></html>", nullptr));
        Note->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:20px; font-weight:700; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">NOTE:</span></p></body></html>", nullptr));
        pocetNoteSpin->setSuffix(QString());
        pocetNoteSpin->setPrefix(QString());
        ctvrtina->setText(QString());
        osmina->setText(QString());
        triola->setText(QString());
        shuffle->setText(QString());
        SOUND_TEXT->setHtml(QCoreApplication::translate("demo_Metronome", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:17px; font-weight:700; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Traditional</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class demo_Metronome: public Ui_demo_Metronome {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
